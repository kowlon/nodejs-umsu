FROM node:14

WORKDIR /
COPY . .

RUN rm -rf /node_modules
RUN rm -rf /package-lock.json
RUN npm install
RUN npm install nodemon -g
RUN npm install sequelize-cli -g
RUN npm install bcrypt
RUN npm install pm2 -g
# CMD npm run dev
CMD ["pm2-runtime", "src/server.js","-i 2"]