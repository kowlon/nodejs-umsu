var express = require('express');
var route = express();
const { verifySignUp, authJwt } = require("../middleware");

const user = require('../controller/user');
route.get('/user', [authJwt.verifyToken], user.index);
route.post('/user', [
    verifySignUp.checkDuplicateUsernameOrEmail,
    verifySignUp.checkRolesExisted
], user.post);
route.put('/user/:id', [authJwt.verifyToken, authJwt.isAdmin], user.update);
route.delete('/user/:id', [authJwt.verifyToken, authJwt.isModeratorOrAdmin], user.delete);
route.post("/user/signin", user.signin);

const kelas = require('../controller/kelas');
route.get('/kelas', kelas.index);
route.post('/kelas', kelas.post);
route.put('/kelas/:id', kelas.update);
route.delete('/kelas/:id', kelas.delete);

const jurusan = require('../controller/jurusan');
route.get('/jurusan', jurusan.index);
route.post('/jurusan', jurusan.post);
route.put('/jurusan/:id', jurusan.update);
route.delete('/jurusan/:id', jurusan.delete);

const mapel = require('../controller/mata_pelajaran');
route.get('/mapel', mapel.index);
route.post('/mapel', mapel.post);
route.put('/mapel/:id', mapel.update);
route.delete('/mapel/:id', mapel.delete);

module.exports = route;