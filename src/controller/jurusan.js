const jurusan = {};
const models = require('../database/models');
const Jurusan = models.jurusan;

jurusan.index = (req, res) => {
    try {
        Jurusan.findAll()
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Ada error nih !"
                })
            })
    } catch (error) {
        res.status(500).send({
            message: err.message || "Unxpected error!"
        })
    }

}

jurusan.post = async (req, res) => {
    let { nama_jurusan } = req.body;

    let data = {
        nama_jurusan
    }

    Jurusan.create(data)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Ada error nih !"
            })
        });
}

jurusan.update = async (req, res) => {
    try {
        let userData = await Jurusan.findOne({
            id: req.params.id, raw: true
        })

        if (req.params.id && userData) {

            let data = {
                nama_jurusan: req.body.nama_jurusan ? req.body.nama_jurusan : userData.nama_jurusan,
            }

            const updateuser = await Jurusan.update(data, {
                where: { id: req.params.id }
            })
                .then(num => {
                    if (num == 1) {
                        res.send({
                            message: `jurusan id ${req.params.id} berhasil Di Update`
                        })
                    } else {
                        res.send({
                            message: `jurusan id ${req.params.id} gagal Di Update`
                        })
                    }
                })
                .catch(err => {
                    res.status(500).send({
                        message: "error updating jurusan" + req.params.id
                    })
                })
        }
    } catch (error) {
        res.status(500).send({
            message: "error updating jurusan" + req.params.id
        })
    }
}

jurusan.delete = (req, res) => {
    const id = req.params.id;

    Jurusan.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Jurusan was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Jurusan with id=${id}. Maybe jurusan was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete jurusan with id=" + id
            });
        });
}


module.exports = jurusan;