const user = {};
const models = require('../database/models');
// const config = require('../database/config/auth.config');
const User = models.user;
const Role = models.role;
const Op = models.Sequelize.Op;
var jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');

user.index = (req, res) => {
    try {
        User.findAll()
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Ada error nih !"
                })
            })
    } catch (error) {
        res.status(500).send({
            message: err.message || "Unxpected error!"
        })
    }

}

user.post = async (req, res) => {
    let { username, password, email } = req.body;
    let hash = await bcrypt.hash(password, 10);

    let data = {
        username,
        password: hash,
        email
    }

    User.create(data)
        .then(user => {
            if (req.body.roles) {
                Role.findAll({
                    where: {
                        name: {
                            [Op.or]: req.body.roles
                        }
                    }
                }).then(roles => {
                    console.log(user);
                    user.setRoles(roles).then(() => {
                        res.send({ message: "User registered successfully!" });
                    });
                });
            } else {
                // user role = 1
                user.setRoles([1]).then(() => {
                    res.send({ message: "User registered successfully!" });
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Ada error nih !"
            })
        });
}

user.update = async (req, res) => {
    try {
        let userData = await User.findOne({
            id: req.params.id, raw: true
        })

        if (req.params.id && userData) {

            let { username, password, email } = req.body;
            let hash = await bcrypt.hash(password, 10);

            let data = {
                username: username ? username : userData.username,
                password: password ? hash : userData.password,
                email: email ? email : userData.email
            }

            const updateuser = await User.update(data, {
                where: { id: req.params.id }
            })
                .then(num => {
                    if (num == 1) {
                        res.send({
                            message: `user id ${req.params.id} berhasil Di Update`
                        })
                    } else {
                        res.send({
                            message: `user id ${req.params.id} gagal Di Update`
                        })
                    }
                })
                .catch(err => {
                    res.status(500).send({
                        message: "error updating user" + req.params.id
                    })
                })
        }
    } catch (error) {
        res.status(500).send({
            message: "error updating user" + req.params.id
        })
    }
}

user.delete = (req, res) => {
    const id = req.params.id;

    User.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "User was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Kelas with id=${id}. Maybe user was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete user with id=" + id
            });
        });
}
user.signin = (req, res) => {
    User.findOne({
        where: {
            username: req.body.username
        }
    })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "User Not found." });
            }

            var passwordIsValid = bcrypt.compareSync(
                req.body.password,
                user.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Invalid Password!"
                });
            }

            var token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, {
                expiresIn: 86400 // 24 hours
            });

            var authorities = [];
            user.getRoles().then(roles => {
                for (let i = 0; i < roles.length; i++) {
                    authorities.push("ROLE_" + roles[i].name.toUpperCase());
                }
                res.status(200).send({
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    roles: authorities,
                    accessToken: token
                });
            });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
}

module.exports = user;