const kelas = {};
const models = require('../database/models');
const Kelas = models.kelas;
const Jurusan = models.jurusan;
const Mata_pelajaran = models.mata_pelajaran;

kelas.index = (req, res) => {
    try {
        Kelas.findAll({ include: [Jurusan] })
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Ada error nih !"
                })
            })
    } catch (error) {
        res.status(500).send({
            message: err.message || "Unxpected error!"
        })
    }

}

kelas.post = async (req, res) => {
    let { nama_kelas, jurusan_id } = req.body;

    let data = {
        nama_kelas,
        jurusan_id
    }

    Kelas.create(data)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Ada error nih !"
            })
        });
}

kelas.update = async (req, res) => {
    try {
        let userData = await Kelas.findOne({
            id: req.params.id, raw: true
        })

        if (req.params.id && userData) {

            let data = {
                nama_kelas: req.body.nama_kelas ? req.body.nama_kelas : userData.nama_kelas,
                jurusan_id: req.body.jurusan_id ? req.body.jurusan_id : userData.jurusan_id,
            }

            const updateuser = await Kelas.update(data, {
                where: { id: req.params.id }
            })
                .then(num => {
                    if (num == 1) {
                        res.send({
                            message: `kelas id ${req.params.id} berhasil Di Update`
                        })
                    } else {
                        res.send({
                            message: `kelas id ${req.params.id} gagal Di Update`
                        })
                    }
                })
                .catch(err => {
                    res.status(500).send({
                        message: "error updating kelas" + req.params.id
                    })
                })
        }
    } catch (error) {
        res.status(500).send({
            message: "error updating kelas" + req.params.id
        })
    }
}

kelas.delete = (req, res) => {
    const id = req.params.id;

    Kelas.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Kelas was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Kelas with id=${id}. Maybe kelas was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete kelas with id=" + id
            });
        });
}


module.exports = kelas;