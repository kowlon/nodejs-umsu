const mata_pelajaran = {};
const models = require('../database/models');
const Mata_pelajaran = models.mata_pelajaran;

mata_pelajaran.index = (req, res) => {
    try {
        Mata_pelajaran.findAll()
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Ada error nih !"
                })
            })
    } catch (error) {
        res.status(500).send({
            message: err.message || "Unxpected error!"
        })
    }

}

mata_pelajaran.post = async (req, res) => {
    let { nama_mapel } = req.body;

    let data = {
        nama_mapel
    }

    Mata_pelajaran.create(data)
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Ada error nih !"
            })
        });
}

mata_pelajaran.update = async (req, res) => {
    try {
        let userData = await Mata_pelajaran.findOne({
            id: req.params.id, raw: true
        })

        if (req.params.id && userData) {

            let data = {
                nama_mapel: req.body.nama_mapel ? req.body.nama_mapel : userData.nama_mapel,
            }

            const updateuser = await Mata_pelajaran.update(data, {
                where: { id: req.params.id }
            })
                .then(num => {
                    if (num == 1) {
                        res.send({
                            message: `mata_pelajaran id ${req.params.id} berhasil Di Update`
                        })
                    } else {
                        res.send({
                            message: `mata_pelajaran id ${req.params.id} gagal Di Update`
                        })
                    }
                })
                .catch(err => {
                    res.status(500).send({
                        message: "error updating mata_pelajaran" + req.params.id
                    })
                })
        }
    } catch (error) {
        res.status(500).send({
            message: "error updating mata_pelajaran" + req.params.id
        })
    }
}

mata_pelajaran.delete = (req, res) => {
    const id = req.params.id;

    Mata_pelajaran.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Mata_pelajaran was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Mata_pelajaran with id=${id}. Maybe mata_pelajaran was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete mata_pelajaran with id=" + id
            });
        });
}


module.exports = mata_pelajaran;