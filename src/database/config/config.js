require('dotenv').config()

const user = process.env.DB_USER;
const host = process.env.DB_HOST;
const pass = process.env.DB_PASSWORD;
const dialect = process.env.DB_DIALECT;
const db_name = process.env.DB_NAME;

module.exports = {
    "username": user,
    "password": pass,
    "database": db_name,
    "host": host,
    "dialect": dialect
}
