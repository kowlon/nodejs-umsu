'use strict';
const config = require('../config/config');
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const db = {};

let sequelize;

sequelize = new Sequelize({
  username: config.username,
  password: config.password,
  database: config.database,
  host: config.host,
  dialect: config.dialect
});

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

//Associations / Join
db.jurusan = require("./jurusan")(sequelize, Sequelize);
db.kelas = require("./kelas")(sequelize, Sequelize);
db.mata_pelajaran = require("./mata_pelajaran")(sequelize, Sequelize);
db.user = require("./user")(sequelize, Sequelize);
db.role = require("./role")(sequelize, Sequelize);

db.jurusan.hasOne(db.mata_pelajaran, {
  foreignKey: 'jurusan_id'
});

db.kelas.belongsTo(db.jurusan, {
  foreignKey: 'jurusan_id'
});

db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "role_id",
  otherKey: "user_id"
});

db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "user_id",
  otherKey: "role_id"
});

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;
