'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let hash = await bcrypt.hash("password", 10);
    return queryInterface.bulkInsert('users', [{
      username: "admin",
      password: hash,
      email: "admin@gmail.com",
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', [{
      username: "admin"
    }])
  }
};
