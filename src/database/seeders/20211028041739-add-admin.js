'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let hash = await bcrypt.hash("administrator", 10);
    return queryInterface.bulkInsert('users', [{
      username: "administrator",
      password: hash,
      email: "administrator@gmail.com",
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
