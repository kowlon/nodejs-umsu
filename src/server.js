require('dotenv').config()
const express = require('express');
const app = express();
const routes = require('./routes/routes');


app.set('port', process.env.PORT || 3000);
app.use(express.json());
app.use(express.urlencoded());

app.use(function (req, res, next) {
    res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
    );
    next();
});

app.get("/", (req, res) => {
    res.json({ message: "Universitas Muhammadiyah Sumatera Utara API V.0.0.1 Beta" });
});
app.use('/', routes);

app.get("/ping", (req, res) => {
    console.log("Pong " + new Date());
    res.json({
        message: "Pong " + new Date(),
        unique_id: process.env.unique_id,
        node_version: process.env.node_version,
        machine_id: process.env.pm_id,
        pm2_version: process.env._pm2_version
    });
});
const db = require("./database/models");
db.sequelize.sync();

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    res.json({ message: "Mau kemana ?" });
});

app.listen(app.get('port'), () => {
    console.log("Start server on port " + app.get('port'))
})